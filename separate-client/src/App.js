import React, { Component } from 'react';
import { FetchData } from './components/FetchData';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <FetchData/>
    );
  }
}
